package sk.svkit.spring.cloud.kubernetes.loadbalancer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "callme-service")
public interface CallmeClient {

    @GetMapping("/")
    Integer numberOfCalls();
}

package sk.svkit.spring.cloud.kubernetes.loadbalancer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoadBalancedClientController {

    @Autowired
    CallmeClient callmeClient;

    @GetMapping("/")
    public Integer numberOfCalls() {
        return callmeClient.numberOfCalls();
    }
}
